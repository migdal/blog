FROM php:7.2-apache

RUN apt-get update
RUN apt-get install -y --no-install-recommends \
    curl \
    libcurl4-openssl-dev \
    libmcrypt-dev \
    libxml2-dev \
    libzip-dev \
    zlib1g-dev \
    libbz2-dev \
    sudo \
    nano

RUN docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd

RUN docker-php-ext-install -j$(nproc) \
    bcmath \
    intl \
    opcache \
    pdo_mysql \
    mysqli \
    zip \
    bz2

RUN apt-get update && apt-get install -y cron

RUN yes | pecl install apcu apcu_bc-beta \
    && docker-php-ext-enable apcu

RUN curl -L 'https://getcomposer.org/installer' -o /tmp/composer-setup.php \
    && php /tmp/composer-setup.php -- --install-dir=/usr/local/bin --filename=composer \
    && rm /tmp/composer-setup.php

RUN mkdir /var/www/.composer
RUN chown www-data /var/www/.composer
RUN chmod -R 775 /var/www/.composer

COPY ./php.ini /usr/local/etc/php/conf.d/php.ini
COPY ./vhost-config.conf /etc/apache2/sites-available/000-default.conf

RUN a2enmod rewrite
RUN a2enmod headers

RUN usermod -a -G www-data www-data
RUN mkdir -p /var/www/html
WORKDIR /var/www/html
ADD code.tar.gz /var/www/html
RUN chown -R www-data:www-data /var/www/html

RUN composer install -o -a
RUN php bin/console c:clear
RUN php bin/console c:w
RUN chown -R www-data:www-data /var/www/html/var

EXPOSE 80

COPY ./entrypoint.sh /usr/local/bin/
CMD /bin/bash /usr/local/bin/entrypoint.sh
