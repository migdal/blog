<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PromotedVideo extends Constraint
{
    public $message = 'Tylko jedno video może być promowane na stronie głównej.';
}