<?php

namespace App\Validator\Constraints;

use App\Repository\ArticleRepository;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PromotedVideoValidator extends ConstraintValidator
{
    /** @var ArticleRepository $articleRepository */
    private $articleRepository;

    /**
     * PromotedVideoValidator constructor.
     * @param ArticleRepository $articleRepository
     */
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function validate($value, Constraint $constraint)
    {
        $existingPromotedArticle = $this->articleRepository->getPromotedArticle();

        if (!$constraint instanceof PromotedVideo) {
            throw new UnexpectedTypeException($constraint, PromotedVideo::class);
        }

        if ($this->context->getObject() === $existingPromotedArticle) {
            return true;
        }

        if (true === $value && !is_null($existingPromotedArticle)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}