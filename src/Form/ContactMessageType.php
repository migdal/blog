<?php

namespace App\Form;

use App\Model\ContactMessage;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactMessageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject', ChoiceType::class, [
                'label' => 'WYBIERZ TEMAT WIADOMOŚCI*',
                'placeholder' => 'Wybierz z listy',
                'required' => true,
                'choices' => [
                    'Porady i sugestie' => 'Porady i sugestie',
                    'Niedziałające video' => 'Niedziałające video',
                    'Pytanie do materiałów na stronie' => 'Pytanie do materiałów na stronie',
                    'Inne' => 'Inne',

                ]
            ])
            ->add('email', TextType::class, [
                'label' => 'ADRES EMAIL*',
                'required' => true
            ])
            ->add('message', TextareaType::class, [
                'label' => 'TREŚĆ WIADOMOŚCI*',
                'attr' => [
                    'rows' => 10
                ],
                'required' => true
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => true,
            'data_class' => ContactMessage::class,
            'allow_extra_fields' => false
        ]);
    }
}