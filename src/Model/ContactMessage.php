<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ContactMessage
{
    /**
     * @var string $subject
     * @Assert\NotBlank
     */
    protected $subject;

    /**
     * @var string $email
     * @Assert\NotBlank
     * @Assert\Email
     */
    protected $email;

    /**
     * @var string $message
     * @Assert\NotBlank
     */
    protected $message;

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ContactMessage
     */
    public function setEmail(string $email): ContactMessage
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ContactMessage
     */
    public function setMessage(string $message): ContactMessage
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return ContactMessage
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }
}