<?php

namespace App\Listeners\Event;

use App\Entity\Article;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\RequestStack;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * Class AssetLoaderEventListener
 */
class AssetLoaderEventListener
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var UploaderHelper
     */
    protected $vichUploaderHelper;

    /**
     * AssetLoaderEventListener constructor.
     * @param RequestStack $requestStack
     * @param UploaderHelper $vichUploaderHelper
     */
    public function __construct(RequestStack $requestStack, UploaderHelper $vichUploaderHelper)
    {
        $this->requestStack = $requestStack;
        $this->vichUploaderHelper = $vichUploaderHelper;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->setUrls($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->setUrls($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    protected function setUrls(LifecycleEventArgs $args)
    {
        if (null === $this->requestStack->getCurrentRequest()) {
            return;
        }

        $entity = $args->getObject();

        switch (true) {
            case ($entity instanceof Article):
                $this->setFileUrl($entity);
                $this->setImageUrl($entity);

                break;
        }
    }

    /**
     * @param Article $step
     */
    protected function setImageUrl($step)
    {
        if (!$step->getImageName()) {
            return;
        }

        $step->setImageUrl(
            $this->vichUploaderHelper->asset($step, 'imageFile')
        );
    }

    /**
     * @param Article $step
     */
    protected function setFileUrl($step)
    {
        if (!$step->getFileName()) {
            return;
        }

        $step->setFileUrl(
            $this->vichUploaderHelper->asset($step, 'file')
        );
    }
}