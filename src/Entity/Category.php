<?php

namespace App\Entity;

use App\Entity\Traits\IdentityTrait;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="category")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity({"title"})
 */
class Category
{
    use IdentityTrait,
        TimestampableTrait;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     */
    protected $title;

    /**
     * @var ArrayCollection $articles
     *
     * @ORM\OneToMany(targetEntity="Article", mappedBy="category")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    protected $articles;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->articles = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Category
     */
    public function setTitle(string $title): Category
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getArticles(): ?Collection
    {
        return $this->articles;
    }

    /**
     * @param Article $article
     * @return Category
     */
    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles->add($article);
            $article->setCategory($this);
        }

        return $this;
    }

    /**
     * @param Article $article
     * @return Category
     */
    public function removeArticle(Article $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
        }

        return $this;
    }
}
