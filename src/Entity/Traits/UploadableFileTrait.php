<?php

namespace App\Entity\Traits;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait UploadableFileTrait
 * @package App\Entity\Traits
 */
trait UploadableFileTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=64, nullable=true)
     */
    protected $fileName;

    /**
     * @var string
     *
     * @ORM\Column(name="file_url", type="string", length=255, nullable=true)
     */
    protected $fileUrl;

    /**
     * @Vich\UploadableField(mapping="app_media", fileNameProperty="fileName")
     *
     * @Assert\File(
     *     maxSize = "1024M",
     *     mimeTypes = {"video/mp4"},
     *     mimeTypesMessage = "Please upload a valid Video file, accepted extensions *.mp4"
     * )
     *
     * @var File
     */
    protected $file;

    /**
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param $fileName
     * @return UploadableFileTrait
     */
    public function setFileName($fileName): self
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFileUrl(): ?string
    {
        return $this->fileUrl;
    }

    /**
     * @param $fileUrl
     * @return UploadableFileTrait
     */
    public function setFileUrl($fileUrl): self
    {
        $this->fileUrl = $fileUrl;
        return $this;
    }

    /**
     *
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File $file
     * @throws \Exception
     */
    public function setFile(File $file)
    {
        $this->file = $file;
        return $this;
    }
}
