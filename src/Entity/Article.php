<?php

namespace App\Entity;

use App\Entity\Traits\IdentityTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\UploadableFileTrait;
use App\Entity\Traits\UploadableImageTrait;
use Doctrine\ORM\Mapping as ORM;
use App\Validator\Constraints as ArticleAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @ORM\Table(name="article")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 * @UniqueEntity({"title"})
 */
class Article
{
    use IdentityTrait,
        TimestampableTrait,
        UploadableFileTrait,
        UploadableImageTrait;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     * @Assert\NotBlank
     */
    protected $title;

    /**
     * @var string $description
     *
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank
     */
    protected $description;

    /**
     * @var boolean $promoted
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @ArticleAssert\PromotedVideo
     */
    protected $promoted;

    /**
     * @var Category $category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="articles")
     * @ORM\JoinColumn(name="category", referencedColumnName="id", onDelete="SET NULL")
     * @Assert\NotBlank
     */
    protected $category;

    /**
     * @var \DateTime $publishDate
     *
     * @ORM\Column(type="datetime")
     */
    protected $publishDate;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->title;
    }

    /**
     * Article constructor.
     */
    public function __construct()
    {
        $this->publishDate = new \DateTime();
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Article
     */
    public function setTitle(string $title): Article
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Article
     */
    public function setDescription(string $description): Article
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isPromoted(): ?bool
    {
        return $this->promoted;
    }

    /**
     * @param bool $promoted
     * @return Article
     */
    public function setPromoted(bool $promoted): Article
    {
        $this->promoted = $promoted;
        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Article
     */
    public function setCategory(Category $category): Article
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublishDate(): \DateTime
    {
        return $this->publishDate;
    }

    /**
     * @param \DateTime $publishDate
     * @return Article
     */
    public function setPublishDate(\DateTime $publishDate): Article
    {
        $this->publishDate = $publishDate;
        return $this;
    }
}