<?php

namespace App\Mailer;

use App\Model\ContactMessage;

class AppMailer
{
    /** @var \Swift_Mailer $mailer */
    protected $mailer;

    /** @var \Twig_Environment $templating */
    protected $templating;

    /** @var string $from */
    protected $from;

    /** @var string $to */
    protected $to;

    /**
     * AppMailer constructor.
     * @param $to
     * @param $from
     * @param \Swift_Mailer $mailer
     * @param \Twig_Environment $templating
     */
    public function __construct($to, $from, \Swift_Mailer $mailer, \Twig_Environment $templating)
    {
        $this->to = $to;
        $this->from = $from;
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    /**
     * @param ContactMessage $message
     * @return int
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function send(ContactMessage $message)
    {
        $message = (new \Swift_Message('Message from website'))
            ->setContentType('text/html')
            ->setFrom([$this->from => 'Website Message'])
            ->setTo($this->to)
            ->setBody($this->templating->render(
                'frontend/contact/message.html.twig',
                compact('message')
            ))
        ;

        return $this->mailer->send($message);
    }
}
