<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

class ArticleRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Article::class);
    }

    /**
     * @return QueryBuilder
     */
    private function commonConditions(): QueryBuilder
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('a')
            ->from($this->_entityName, 'a')
        ;

        return $qb;
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPromotedArticle()
    {
        $qb = $this->commonConditions();
        $qb
            ->where('a.promoted = :yes')
            ->setParameter('yes', true);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return mixed
     */
    public function getArticles()
    {
        $qb = $this->commonConditions();
        $qb
            ->orderBy('a.publishDate', 'DESC')
            ->addOrderBy('a.category', 'DESC')
            ->addOrderBy('a.title', 'DESC')
        ;

        return $qb->getQuery()->getResult();
    }
}
