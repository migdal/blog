<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class CategoryRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Category::class);
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('c')
            ->from($this->_entityName, 'c')
            ->orderBy('c.createdAt', 'ASC')
        ;

        return $qb->getQuery()->getResult();
    }
}
