<?php

namespace App\Admin;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\DateRangeFilter;
use Sonata\Form\Type\DatePickerType;
use Sonata\Form\Type\DateTimePickerType;
use Sonata\Form\Type\DateTimeRangePickerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;


class ArticleAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected $baseRouteName = 'article_admin';

    /**
     * {@inheritdoc}
     */
    protected $baseRoutePattern = 'article';

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();

        $fileRequired = false;
        if($subject && $subject->getId() && $subject->getFileName()){
            $fileRequired = false;
        }

        $imageRequired = false;
        if($subject && $subject->getId() && $subject->getImageName()){
            $imageRequired = false;
        }

        $formMapper
            ->tab('Główne')
                ->with('Główne', ['class' => 'col-md-6'])
                    ->add('title', TextType::class, [
                        'label' => 'Tytuł',
                        'required' => true
                    ])
                    ->add('category', null, [
                        'label' => 'Kategoria',
                        'required' => true
                    ])
                    ->add('publishDate', DatePickerType::class, [
                        'label' => 'Data publikacji',
                        'format' => 'yyyy-MM-dd'
                    ])
                    ->add('promoted', null, [
                        'label' => 'Promowany na stronie głownej',
                    ])
                ->end()
                ->with('Plik Video', ['class' => 'col-md-6'])
                    ->add('file', VichFileType::class, [
                        'label' => 'Video',
                        'required' => $fileRequired
                    ])
                ->end()
                ->with('Zaślepka', ['class' => 'col-md-6'])
                    ->add('imageFile', VichImageType::class, [
                        'label' => 'Zaślepka',
                        'required' => $imageRequired
                    ])
                ->end()
            ->end()
            ->tab('Treść')
                ->with('Treść', ['class' => 'col-md-12'])
                    ->add('description', CKEditorType::class, [
                        'label' => 'Opis',
                    ])
                ->end()
            ->end();
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, [
                'label' => 'Tytuł'
            ])
            ->add('createdAt', DateRangeFilter::class, [
                'label' => 'Data utworzenia',
                'field_type' => DateTimeRangePickerType::class
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, [
                'label' => 'Tytuł'
            ])
            ->add('category', null, [
                'label' => 'Kategoria'
            ])
            ->add('promoted', null, [
                'label' => 'Promowany'
            ])
            ->add('publishDate', null, [
                'label' => 'Data publikacji'
            ])
            ->add('updatedAt', null, [
                'label' => 'Data modyfikacji'
            ])
            ->add(
                '_action',
                'actions',
                [
                    'header_style' => 'width: 15%; text-align: center',
                    'row_align' => 'center',
                    'label' => 'Akcje',
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ]
            )
        ;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('export');
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        parent::configure();

        $this->datagridValues['_sort_by'] = 'createdAt';
        $this->datagridValues['_sort_order'] = 'DESC';
    }
}
