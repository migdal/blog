<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @param ArticleRepository $articleService
     * @Route("/", name="frontend_homepage")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ArticleRepository $articleService)
    {
        return $this->render('frontend/main/view.html.twig', [
            'promotedArticle' => $articleService->getPromotedArticle(),
        ]);
    }
}