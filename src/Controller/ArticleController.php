<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @param ArticleRepository $articleRepository
     * @param CategoryRepository $categoryRepository
     * @Route("/artykuly", name="frontend_articles")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(ArticleRepository $articleRepository, CategoryRepository $categoryRepository)
    {
        return $this->render('frontend/article/list.html.twig', [
            'categories' => $categoryRepository->getCategories(),
            'articles' => $articleRepository->getArticles(),
        ]);
    }
}
