<?php

namespace App\Controller;

use App\Form\ContactMessageType;
use App\Mailer\AppMailer;
use App\Model\ContactMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @param Request $request
     * @param AppMailer $mailer
     * @Route("/kontakt", name="frontend_contact")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index(Request $request, AppMailer $mailer)
    {
        $form = $this->createForm(ContactMessageType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mailer->send($form->getData());
            $this->addFlash('success', 'Wiadomość została wysłana');
        }

        return $this->render('frontend/contact/view.html.twig', [
            'form' => $form->createView(),
        ]);
    }

//    /**
//     * @Route("/send", name="frontend_contact_send", methods={"POST"})
//     * @return \Symfony\Component\HttpFoundation\Response
//     */
//    public function send()
//    {
//
//    }
}
