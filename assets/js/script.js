const $ = require('jquery');
window.$ = $;
window.jQuery = $;
require('bootstrap');

$(document).ready(function () {
    $("#videoBtn").click(function(){
        $("#videoModal").modal({backdrop: 'static'});
    });
    $("#videoListBtn").click(function(){
        $("#videoModalfromList").modal({backdrop: 'static'});
    });

    $(".show_cat").hide();
    $('.show_cat_1').show();
    $('.cat_id').removeClass("active");
    $('.cat_id_1').addClass("active");

    $('.category-link').click(function () {
       var categoryId = $(this).data('categoryid');
        $('.show_cat').hide();
        $('.show_cat_' + categoryId).show();
        $('.cat_id').removeClass("active");
        if($(this).data('clicked', true) && categoryId) {
            //console.log("clicked");
            $('.cat_id_' + categoryId).addClass("active");
        }
    });

    $('.close-main-video').on('click', function () {
        document.getElementById("main-video").pause();
    });
    $('.close-modal-video').on('click', function () {
        document.getElementById("modal-video").pause();
    });

});

/*cookies code start*/
var index = {
    manageCookies: function() {
        var Cookies = require('js-cookie');
        var isCookie = Cookies.get('cookies');
        if (isCookie == undefined) {
            $('.cookiesWrapper').css('display', 'block');
        }
        $('.closeIcon').click(function() {
            Cookies.set('cookies', 'true', {
                expires: 30,
                path: ''
            });
            $('.cookiesWrapper').hide(600);
        });
        $('.agreeLink').click(function(event) {
            event.preventDefault();
            Cookies.set('cookies', 'true', {
                expires: 30,
                path: ''
            });
            $('.cookiesWrapper').hide(600);
        });
    }
};

$(document).ready(function($) {
    index.manageCookies();
});

(function(factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        var _OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function() {
            window.Cookies = _OldCookies;
            return api;
        };
    }
}(function() {
    function extend() {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[i];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }

    function init(converter) {
        function api(key, value, attributes) {
            var result;

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}

                value = encodeURIComponent(String(value));
                value = value.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
                    attributes.path && '; path=' + attributes.path,
                    attributes.domain && '; domain=' + attributes.domain,
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var name = parts[0].replace(rdecode, decodeURIComponent);
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    cookie = converter && converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }

            return result;
        }

        api.get = api.set = api;
        api.getJSON = function() {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function(key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }

    return init();

}));
/*cookies code end*/